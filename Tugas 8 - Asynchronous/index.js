// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini

console.log('Soal No. 1 (Callback Baca Buku)');


for (i = 0; i < books.length; i++) {
    readBooks(books[i], books[i], function() {}) 
}

