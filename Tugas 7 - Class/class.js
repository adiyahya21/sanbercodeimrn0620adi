console.log('1. Animal Class');
console.log('Release 0');

class Animal {
    // Code class di sini
    constructor(name,legs,cold_blooded) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
    get name() {
        return this._name;
    }
    set name(x) {
        this._name = x;
    }

    get legs() {
        return this._legs;
    }
    set legs(x) {
        this._legs = 4;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
    set cold_blooded(x) {
        this._cold_blooded = false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\nRelease 1');
// Code class Ape dan class Frog di sini
class Ape {
    constructor(strvar) {
        this.strvar = strvar;
    }

    yell() {
        return "Auooo";
    }
}

class Frog {
    constructor(strvar) {
        this.strvar = strvar;
    }

    jump() {
        return "hop hop";
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()); // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()); // "hop hop"


console.log('\n2. Function to Class');
class Clock {
    constructor({ template }) {
        this.template = template;
        
    }

    render() {
        this.date = new Date();
        this.hours = date.getHours();
        this.mins = date.getMinutes();
        this.secs = date.getSeconds();
        this.output = template.replace('h', hours).replace('m', mins).replace('s', secs);

        if (hours < 10) hours = '0' + hours;
        if (mins < 10) mins = '0' + mins;
        if (secs < 10) secs = '0' + secs;
        return this.output;
    }

    start() {
        setInterval(() => this.render(),1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
console.log(clock.start());