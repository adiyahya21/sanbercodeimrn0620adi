// If-else
var nama = "John"
var peran = "werewolf"

if(nama==''){
    console.log("Nama harus diisi!");
}else if(nama!='' && peran==''){
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
}else{
    
    console.log("Selamat datang di Dunia Werewolf, "+nama);

    if(peran=='penyihir'){
        console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if(peran=='guard'){
        console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }else if(peran=='werewolf'){
        console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!" );
    };
};

console.log('\n');
// Switch Case
var hari = 21; 
var bulan = 5; 
var tahun = 1945;
const months = ["Jan", "Feb", "Mar","Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nop", "Des"];

switch(bulan) {
    case 1: { var strbulan = "Januari" ; break; }
    case 2: { var strbulan = "Februari" ; break; }
    case 3: { var strbulan = "Maret" ; break; }
    case 4: { var strbulan = "April" ; break; }
    case 5: { var strbulan = "Mei" ; break; }
    case 6: { var strbulan = "Juni" ; break; }
    case 7: { var strbulan = "Juli" ; break; }
    case 8: { var strbulan = "Agustus" ; break; }
    case 9: { var strbulan = "September" ; break; }
    case 10: { var strbulan = "Oktober" ; break; }
    case 11: { var strbulan = "Nopember" ; break; }
    case 12: { var strbulan = "Desember" ; break; }
}
console.log(hari + " " + strbulan + " " + tahun);