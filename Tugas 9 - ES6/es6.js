console.log('1. Mengubah fungsi menjadi fungsi arrow');
const golden = () => {
    console.log("this is golden!!")
}
golden()

console.log('\n2. Sederhanakan menjadi Object literal di ES6')
const tesFullName = {
    firstName: 'William',
    lastName: 'Imoh',
    fullName: function(){ 
        return `${this.firstName} ${this.lastName}`
    }
}
console.log(tesFullName.fullName());

console.log('\n3. Destructuring');
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation);

console.log('\n4. Array Spreading');
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

console.log('\n5. Template Literals')

const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolor sit amet,
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before) 