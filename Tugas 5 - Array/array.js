console.log('Soal No. 1 (Range)');

function range(startNum,finishNum){
    if(startNum && finishNum){
        var StrArr = [];
        if(startNum<=finishNum){
            for(var NoUrut=startNum; NoUrut<=finishNum; NoUrut++){
                StrArr.push(NoUrut);
            }; 
        }else{
            for(var NoUrut=startNum; NoUrut >= finishNum; NoUrut--){
                StrArr.push(NoUrut);
            };
        };
    }else{
        var StrArr = -1;
    };
    return StrArr;
};

console.log(range(1, 10))       // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1))           // -1
console.log(range(11,18))       // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50))      // [54, 53, 52, 51, 50]
console.log(range())            // -1 

console.log('\n');
console.log('Soal No. 2 (Range with Step)');
function rangeWithStep(startNum, finishNum, step){
    var StrArr = [];
    if(startNum<=finishNum){
        var StrNoUrut = startNum;
        while(StrNoUrut <= finishNum){
            StrArr.push(StrNoUrut);
            var StrNoUrut = StrNoUrut+step;
        }
    }else{
        var StrNoUrut = startNum;
       
        while(StrNoUrut >= finishNum){
            StrArr.push(StrNoUrut);
            var StrNoUrut = StrNoUrut-step;
        }
       
    };
    return StrArr;
};
console.log(rangeWithStep(1, 10, 2))    // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3))   // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1))     // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4))    // [29, 25, 21, 17, 13, 9, 5]

console.log('\n');
console.log('Soal No. 3 (Sum of Range)');

function sum(awal, akhir, jarak){
    var StrArr = 0;

    if(awal && akhir){
        if(jarak){
            var jarak = jarak;
        }else{
            var jarak = 1;
        };
        
        if(akhir > awal){    
            var StrNoUrut = awal;
            while(StrNoUrut <= akhir){
                var StrArr = StrArr + StrNoUrut;
                var StrNoUrut = StrNoUrut+jarak;
            };
        }else{
            var StrNoUrut = akhir;
            while(StrNoUrut <= awal){
                var StrArr = StrArr + StrNoUrut;
                var StrNoUrut = StrNoUrut+jarak;
            };
        };
    }else if(awal){
        return StrArr = 1;
    }else{
        return StrArr = 0;
    };  
    
    return StrArr;
};

console.log(sum(1,10))          // 55
console.log(sum(5, 50, 2))      // 621
console.log(sum(15,10))         // 75
console.log(sum(20, 10, 2))     // 90
console.log(sum(1))             // 1
console.log(sum())              // 0 


console.log('\n');
console.log('Soal No. 4 (Array Multidimensi)');

function dataHandling(input){
    var ln = input.length;
    var StrArr = "";
    for (i=0; i<ln; i++) {
        var StrArrAdd = "\n\nNomor ID: "+input[i][0]+"\nNama Lengkap: "+input[i][1]+"\nTTL: "+input[i][2]+" "+input[i][3]+"\nHobi: "+input[i][4];
        var StrArr = StrArr.concat(StrArrAdd);
    };
    return StrArr;
};

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input));


console.log('\n');
console.log('Soal No. 5 (Balik Kata)');

function balikKata(input){
    var StrArr = input;
    var LnStrArr = StrArr.length-1;
    var StrArrNew = "";
    for (i=LnStrArr; i>=0; i--) {
        var StrArrNew = StrArrNew.concat(StrArr.charAt(i));
    };
    return StrArrNew;
};

console.log(balikKata("Kasur Rusak"))    // kasuR rusaK
console.log(balikKata("SanberCode"))     // edoCrebnaS
console.log(balikKata("Haji Ijah"))      // hajI ijaH
console.log(balikKata("racecar"))        // racecar
console.log(balikKata("I am Sanbers"))   // srebnaS ma I 