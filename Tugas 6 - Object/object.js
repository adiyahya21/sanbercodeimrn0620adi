console.log('Soal No. 1 (Array to Object)');

function arrayToObject(arr) { 

    var NewDate = new Date();
    var FullYear = NewDate.getFullYear();
    var VarResult = "";
    for(i=0; i<arr.length; i++ ){
        var num = i+1;
        if(arr[i][3] && arr[i][3] <= FullYear ){
            var NewAge = FullYear-arr[i][3];
            var Age = "age: "+NewAge;
        }else{
            var Age = "age: Invalid Birth Year";
        }
        var VarResult = VarResult+""+num+". "+arr[i][0]+" "+arr[i][1]+" : { firstName: "+arr[i][0]+", lastName: "+arr[i][1]+", gender: "+arr[i][2]+" ,"+Age+"}\n";
    };  

    return VarResult;
};
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log(arrayToObject(people))
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log('\nSoal No. 2 (Shopping Time)');
function shoppingTime(memberId, money) {
    var listPurchasedObj = {
        "Sepatu brand Stacattu" : 1500000,
        "Baju brand Zoro" : 500000,
        "Baju brand H&N" : 250000,
        "Sweater brand Uniklooh" : 175000,
        "Casing Handphone" : 50000
    };
    var sortablePurchasedObj = [];
    for (var harga in listPurchasedObj) {
        sortablePurchasedObj.push([harga, listPurchasedObj[harga]]);
    };

    if( (!memberId && !money) || (!memberId || !money) ){
        var VarResult = 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }else if(money<50000){
        var VarResult = "Mohon maaf, uang tidak cukup";
    }else{
        var SisaUang = money;
        var BarangDibeliObj = [];
        for(var keyName in sortablePurchasedObj){
            var NamaBarang = sortablePurchasedObj[keyName][0];
            var HargaBarang = sortablePurchasedObj[keyName][1];
            if( HargaBarang <= SisaUang ){
                BarangDibeliObj.push("'"+NamaBarang+"'");
                var SisaUang = SisaUang-HargaBarang;
            };
        };
        var VarResult = "{ memberId: "+memberId+", money: "+money+", listPurchased: ["+BarangDibeliObj+"], changeMoney: "+SisaUang+" }";
    }

    return VarResult;
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log('\nSoal No. 3 (Naik Angkot)');
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var VarResult = "[";
    for(i=0; i<arrPenumpang.length; i++ ){
        var num = i+1;
        var strBayar = 0;
        var totalRute = 0;
        for(j=0; j<rute.length; j++ ){
            if(rute[j]==arrPenumpang[i][1]){
                var strStart = j;
            };
            if(rute[j]==arrPenumpang[i][2]){
                var strEnd = j;
            };
        };
        var strJarak = strEnd-strStart;
        var strBayar = strJarak*2000;
        var VarResult = VarResult+" { Penumpang: '"+arrPenumpang[i][0]+"', naikDari: '"+arrPenumpang[i][1]+"', tujuan: '"+arrPenumpang[i][2]+"', bayar: '"+strBayar+"' } ";
    };  
    var VarResult = VarResult+"]";
    return VarResult;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));