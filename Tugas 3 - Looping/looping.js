// No. 1 Looping While
console.log('No. 1 Looping While');
var flag = 1;
console.log('LOOPING PERTAMA');
while(flag <= 20) {
    flag++;
    console.log(flag+' - I love coding'); 
    flag++; 
}

var flag = 21;
console.log('LOOPING KEDUA');
while(flag >= 2) {
    flag--;
    console.log(flag+' - I will become a mobile developer'); 
    flag--; 
}

console.log('\n');
// No. 2 Looping menggunakan for
console.log('No. 2 Looping menggunakan for');
var flag = 1;
while(flag <= 20) {
    if (flag%2==0) {
        // Jika angka genap maka tampilkan Berkualitas
        console.log(flag+' - Berkualitas');
    }else{
        if(flag%3==0){
            // Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
            console.log(flag+' - I Love Coding'); 
        }else{
            // Jika angka ganjil maka tampilkan Santai
            console.log(flag+' - Santai'); 
        };
    };
    flag++; 
}

console.log('\n');
// No. 3 Membuat Persegi Panjang
console.log('No. 3 Membuat Persegi Panjang');
var flag = 1;
while(flag <= 4) {
    console.log('########'); 
    flag++; 
}

console.log('\n');
// No. 3 Membuat Persegi Panjang
console.log('No. 4 Membuat Tangga');
var flag = 1;
while(flag <= 7) {
    var StrToPrint = "";
    var flag2 = 1;
    while(flag2 <= flag ) {
        var StrToPrint = StrToPrint.concat('#');
        flag2++;     
    };
    console.log(StrToPrint); 
    flag++; 
}

console.log('\n');
// No. 5 Membuat Papan Catur
console.log('No. 5 Membuat Papan Catur');
var flag = 1;
while(flag <= 8) {
    if (flag%2==0) {
        console.log('# # # # ');
    }else{
        console.log(' # # # #'); 
    }
    flag++; 
}